<?php include_once 'templates/header.php';
      include_once 'inc/functions.php';
 ?>
 <section class="maincontent">
 
 <?php 

   if (isset($_POST['calculation'])) {
      $num1 = $_POST["first_number"];
      $num2 = $_POST["second_number"];

      $error = array();

      if (empty($num1) && empty($num2)) {
         $error[] = "Number field must not be empty!";
      }
   }

  ?>
 
   <div class="contaier">
      <div class="row">
         <div class="col-md-12">
            <?php 
               if (isset($error)) {
                  foreach ($error as $errors) {
                     ?>
                        <li class="alert alert-danger"><?php echo $errors; ?></li>
                     <?php 
                  }
               }
             ?>
            <form action="" method="post">
               
               <div class="form-group">
                  <label for="first_number">First Number</label>
                  <input type="number" id="first_number" name="first_number" class="form-control">
               </div>
               <div class="form-group">
                  <label for="second_number">Second Number</label>
                  <input type="number" id="second_number" name="second_number" class="form-control">
               </div>
                  <button class="btn btn-success" name="calculation" type="submit">Calculate</button>
            </form>
            <?php 
               if (isset($_POST['calculation'])) {
                  $cal = new Calculation;
                  $cal->add($num1,$num2);

                  $cal->sub($num1,$num2);
                  
                  $cal->multiplication($num1,$num2);
               }

             ?>
         </div>
      </div>
   </div>
     
   
  </section>
<?php include_once 'templates/footer.php'; ?>

